<?php if($this->beginCache($x . "x" . $y, ['duration' => 3600*24])): ?>

    X: <?= $x ?><br>
    Y: <?= $y ?><br>
    Prod: <?= $x * $y ?><br>
<?php
    $this->endCache();
    endif;
?>
<?php $random = rand(0, 100); ?>
Rand: <?= $random ?><br>
Inc: <?= $inc ?><br>

<?php
$date_format = 'Y-m-d H:i:s';
$arr = [
    'datetime' => date($date_format),
    'x' => $x,
    'y' => $y,
    'prod' => $x*$y,
    'rand' => $random,
    'inc' => $inc,
];
$json_arr = json_encode($arr);
$path = Yii::$app->basePath . '/json/';
file_put_contents($path . date($date_format).$x.$y.$inc, $json_arr);
